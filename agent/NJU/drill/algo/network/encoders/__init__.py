from drill.api.bp.agent.features import *
from drill.api.bp.network import FeatureEncoder
from .common import CommonEncoder
from .entity import EntityEncoder
from .spatial import SpatialEncoder
from .bypass import BypassEncoder


def get_encoder_for(feature_template, template_name):
    """
    this method is needed, because we cannot expose core code(default Encoders) in user's API lib.
    :param feature_template: an instance of FeatureTemplate
    :param template_name:
    :return: an instance of FeatureEncoder
    """
    assert isinstance(feature_template, FeatureTemplate)
    if feature_template.encoder is not None:
        assert isinstance(feature_template.encoder, FeatureEncoder)
        return feature_template.encoder
    else:
        if isinstance(feature_template, CommonFeatureTemplate):
            return CommonEncoder(config=feature_template.encoder_config)
        elif isinstance(feature_template, SpatialFeatureTemplate):
            return SpatialEncoder(config=feature_template.encoder_config)
        elif isinstance(feature_template, EntityFeatureTemplate):
            return EntityEncoder(config=feature_template.encoder_config)
        elif isinstance(feature_template, ActionMaskFeatureTemplate):
            return BypassEncoder(config=feature_template.encoder_config)
        else:
            assert False, "no valid FeatureEncoder found for feature_template {}".format(template_name)
