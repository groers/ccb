from agent.NJU.player.formation import Formation, FormationStatus
from env.env_def import UnitType
from env.env_cmd import EnvCmd
from agent.NJU.player.utils import will_attack_by_ship
from env.env_cmd import EnvCmd
from agent.NJU.player.utils import compute_2d_battle_point, compute_2d_distance_point

PLANE_TYPES = [UnitType.A2A, UnitType.A2G, UnitType.AWACS, UnitType.DISTURB]
AREA_PATROL_PARAMS = [270, 1, 1, 250, 10800]


class A2gHuntFormation(Formation):
    MAX_SHIP_HUNT_NUM = 3

    def __init__(self, side, name, st, unit_ob):
        super(A2gHuntFormation, self).__init__(side, name, st, unit_ob)
        self.attack_ship_unit = None

    def make_ship_hunt(self, attack_list):
        cmds = []
        rm_list = []
        if self.unit_ob.id_map[self.en_side].get(self.attack_ship_unit.id) is None:
            print("make_ship_hunt 找不到突袭目标")
            return cmds
        hunting_num = len(attack_list.lists.get(self.attack_ship_unit.id, []))  # 正在突击的轰炸机数目
        a2g_num = max(0, self.MAX_SHIP_HUNT_NUM - hunting_num)  # 突击舰船的轰炸机数目

        print("对舰船的打击列表", attack_list.lists.get(self.attack_ship_unit.id, []))
        print("make_ship_hunt : a2g_num", a2g_num)
        unit_dis_tuples = [(None, 1200000) for i in range(a2g_num)]

        for a2g_id in self.available_unit_ids[UnitType.A2G]:
            unit = self.unit_ob.id_map[self.side][a2g_id]
            dis = unit.compute_2d_distance_unit(self.attack_ship_unit)
            for i in range(a2g_num):
                if dis < unit_dis_tuples[i][1]:
                    unit_dis_tuples[i] = (unit, dis)
                    break
        print("make_ship_hunt : unit_dis_tuples", unit_dis_tuples)
        for unit, _ in unit_dis_tuples:
            if unit is None:
                break
            id_ = unit.id
            en_id = self.attack_ship_unit.id
            cmds.append(
                EnvCmd.make_targethunt(id_, en_id, unit.compute_2d_attack_angle_unit(self.attack_ship_unit), 90))
            rm_list.append(id_)
            self.available_unit_num[UnitType.A2G] -= 1
            attack_list.add_attack(id_, en_id)
        for id_ in rm_list:
            self.available_unit_ids[UnitType.A2G].remove(id_)
        print("make_ship_hunt : hunt cmds", cmds)
        return cmds

    # a2g_hunt_fm的mode没有用
    def step(self, attack_list=None, types=None, mode=0):
        cmds = []
        if attack_list is None:
            return cmds

        if types is None:
            types = [UnitType.COMMAND, UnitType.S2A, UnitType.SHIP]
        print('___________________________________________________________')
        print("a2g step:name", self.name)
        print("a2g step:reside_point", self.reside_point)
        print("a2g step:hunt_st", self.st)

        cmds_ = self.make_patrol_attack(attack_list, types, [])
        print("a2g step: hunt cmds", cmds_)
        cmds.extend(cmds_)
        disturb_id = self.pt_ids[UnitType.DISTURB][0] if len(self.pt_ids[UnitType.DISTURB]) > 0 else -1
        awacs_id = self.pt_ids[UnitType.AWACS][0] if len(self.pt_ids[UnitType.AWACS]) > 0 else -1
        if len(cmds_) is not 0:
            self.st = FormationStatus.TARGET_HUNT
            for id_ in [disturb_id, awacs_id]:
                if id_ is not -1:
                    center_2d_point = self.compute_2d_center_point()
                    cmds.append(EnvCmd.make_areapatrol(id_, *center_2d_point, 8000, *AREA_PATROL_PARAMS))
            return cmds
        arrived = self.has_all_arrived(*self.reside_point)

        if self.st == FormationStatus.AREA_PATROL:
            if not arrived:
                cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
                self.st = FormationStatus.MOVING_2_POINT
            return cmds

        elif self.st == FormationStatus.TARGET_HUNT:
            if self.is_units_available():
                self.st = FormationStatus.MOVING_2_POINT
                cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
                return cmds
            return cmds
        elif self.st == FormationStatus.MOVING_2_POINT:
            if arrived:
                self.st = FormationStatus.AREA_PATROL
                return cmds

            attack_ship_unit, gap_dis = self.get_encounter_ship_unit_and_gap_dis()
            # 如果不会遇到船只，或者距离还太远
            if attack_ship_unit is None or gap_dis > 180000:
                return cmds
            self.st = FormationStatus.SHIP_HUNT
            self.attack_ship_unit = attack_ship_unit
            cmds.extend(self.make_ship_hunt(attack_list))

            # 剩余飞机在原地巡逻
            avai_center_point = self.compute_2d_center_point([UnitType.A2G], 1)
            for _, id_list in self.available_unit_ids.items():
                for id_ in id_list:
                    cmds.append(EnvCmd.make_areapatrol(id_, *avai_center_point, 8000, *AREA_PATROL_PARAMS))

            if disturb_id is not -1:
                battle_2d_point = compute_2d_battle_point(*avai_center_point, attack_ship_unit.x, attack_ship_unit.y,
                                                          122000)
                cmds.append(EnvCmd.make_areapatrol(disturb_id, *battle_2d_point, 8000, *AREA_PATROL_PARAMS))
                print("干扰机距离船", compute_2d_distance_point(*battle_2d_point, attack_ship_unit.x, attack_ship_unit.y))

            return cmds

        elif self.st == FormationStatus.SHIP_HUNT:
            cmds__ = self.make_ship_hunt(attack_list)
            cmds.extend(cmds__)
            if len(cmds__) is 0 and self.is_units_available():
                self.st = FormationStatus.MOVING_2_POINT
                cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
                return cmds
            return cmds  # 保持现状态

        else:
            print("wrong state!!!")
            return cmds
