from agent.NJU.player.formation import Formation, FormationStatus
from env.env_def import UnitType
from env.env_cmd import EnvCmd
from agent.NJU.player.utils import will_attack_by_ship
from env.env_cmd import EnvCmd
from agent.NJU.player.utils import compute_2d_battle_point, compute_2d_distance_point

PLANE_TYPES = [UnitType.A2A, UnitType.A2G, UnitType.AWACS, UnitType.DISTURB]
AREA_PATROL_PARAMS = [270, 1, 1, 250, 10800]


class A2aEscortFormation(Formation):

    # 如果mode=0，那么前进路上会遇到船会原地巡逻等待,直到船被消灭
    def step(self, attack_list=None, types=None, mode=0):
        cmds = []
        if attack_list is None:
            return cmds

        if types is None:
            types = PLANE_TYPES
        # print("----------------")
        # print("a2a step: fm_name", self.name)
        # print("a2a step:reside_point", self.reside_point)
        # print("a2a step: fm_st", self.st)

        cmds_ = self.make_patrol_attack(attack_list, [], types)
        # print("a2a step: attack cmds", cmds_)
        cmds.extend(cmds_)

        if len(cmds_) is not 0:
            self.st = FormationStatus.TARGET_HUNT
            return cmds
        arrived = self.has_all_arrived(*self.reside_point)

        if self.st == FormationStatus.AREA_PATROL:
            if not arrived:
                self.st = FormationStatus.MOVING_2_POINT
                cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
            return cmds

        elif self.st == FormationStatus.TARGET_HUNT:
            if self.is_units_available():
                self.st = FormationStatus.MOVING_2_POINT
                cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
                return cmds
            return cmds

        elif self.st == FormationStatus.MOVING_2_POINT:
            if arrived:
                self.st = FormationStatus.AREA_PATROL
                return cmds
            attack_ship_unit, gap_dis = self.get_encounter_ship_unit_and_gap_dis()
            if mode is 0 and attack_ship_unit is not None and gap_dis < 150000:  # 距离挺远的话可以继续前进
                self.st = FormationStatus.CIRCLING
                cmds.extend(self.make_formation_move(*self.compute_2d_center_point(), 8000, attack_list))
            return cmds

        elif self.st == FormationStatus.CIRCLING:
            attack_ship_unit, gap_dis = self.get_encounter_ship_unit_and_gap_dis()
            if attack_ship_unit is None or gap_dis >= 160000:
                self.st = FormationStatus.MOVING_2_POINT
                cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
            return cmds
        else:
            # print("a2a_escort_fm wrong state!!!", self.name)
            return cmds
