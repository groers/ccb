from env.env_def import UnitType, RED_AIRPORT_ID
from agent.NJU.bp_env.env_cmd import EnvCmd, Point
from agent.NJU.player.unit_ob import UnitOb
from agent.NJU.player.attack_list import AttackList
from agent.NJU.player.formation import Formation, FormationStatus
from agent.NJU.player.a2g_hunt_fm import A2gHuntFormation
from agent.NJU.player.a2a_escort_fm import A2aEscortFormation

from agent.NJU.player.red.parameters import *
from agent.agent import Agent
from copy import deepcopy


class AgentState:
    DEPLOY_STATE = 10  # 部署单位，起飞单位
    FORMATION_STATE = 20  # 将飞机编入编队
    DECISION_STATE = 30  # 决定攻打哪个岛屿
    SOUTH_ATTACK_STATE = 40  # 攻打南岛阶段
    SOUTH_ATTACK_STATE_PARSE1 = 41  # 攻打南岛已经达到南部集合点
    SOUTH_ATTACK_STATE_PARSE2 = 42  # 攻打南岛已经达到南部突击点

    NORTH_ATTACK_STATE = 50  # 攻打北岛阶段
    NORTH_ATTACK_STATE_PARSE1 = 51
    RETREAT_STATE = 60  # 攻岛完毕撤退阶段
    WAITING_FOR_REINFORCEMENT = 70  # 等待支援


class RulePlayer(Agent):
    SOUTH_A2A_TEAM_NUM = 3
    SOUTH_A2G_TEAM_NUM = 5
    NORTH_A2A_TEAM_NUM = 3
    NORTH_A2G_TEAM_NUM = 4
    AWACS_ESCORT_TEAM_NUM = 2

    name_cmds_map = {}  # key为编队名，value为编队包含的单位所执行过的命令

    def __init__(self, name, config):
        super().__init__(name, config['side'])
        self.en_side = 'blue' if config['side'] == 'red' else 'red'
        self.agent_state = AgentState.DEPLOY_STATE

        self.unit_ob = UnitOb(config['side'])
        self.attack_list = AttackList(config['side'])
        self.formation_map = {}  # key为编队名，value为编队
        self.firstly_attack_target = ""  # 首先打击的目标岛屿，'south'或者'north'

        self.awacs_id = 0
        self.disturb_id = 0
        self.ship_ids = [0, 0]
        self.south_command_id = 0
        self.north_command_id = 0

        self.task_formations = []  # 执行攻岛任务的编队
        self.retreat_line = []  # 列表元素为大小为三的子列表代表坐标

    def make_step_update(self, raw_obs):
        cmds = []
        self.unit_ob.update(raw_obs)
        cmds_, return_ids = self.attack_list.make_update(self.unit_ob)  # 新增一个返回值，返回返航集合
        cmds.extend(cmds_)

        rm_formations = []
        for name, formation in self.formation_map.items():
            cmds.extend(formation.update(self.attack_list, self.unit_ob, return_ids))
            if formation.is_empty():
                rm_formations.append(name)

        # # 从编队字典中删除空编队。暂时这么设置，万一以后要留着往其中补充单位呢
        # for name in rm_formations:
        #     del self.formation_map[name]

        if self.disturb_id not in self.unit_ob.ids[self.side][UnitType.DISTURB]:
            self.disturb_id = 0
        if self.awacs_id not in self.unit_ob.ids[self.side][UnitType.AWACS]:
            self.awacs_id = 0
        if self.south_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.south_command_id = 0
        if self.north_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.north_command_id = 0
        return cmds

    # 将单位分进编队里
    def add_unit2formation(self, raw_obs):
        for name, cmds in self.name_cmds_map.items():
            if name not in self.formation_map:
                if name.startswith('a2g'):
                    self.formation_map[name] = A2gHuntFormation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                                self.unit_ob)
                elif name.startswith('a2a'):
                    self.formation_map[name] = A2aEscortFormation(self.side, name, FormationStatus.MOVING_2_POINT,
                                                                  self.unit_ob)
                else:
                    self.formation_map[name] = Formation(self.side, name, FormationStatus.MOVING_2_POINT, self.unit_ob)
            str_cmds = [str(cmd).replace("\'", "\"") for cmd in cmds]  # team['Task']其实是个字符串，要比较的话，需要对cmd做str
            for team in raw_obs['teams']:
                if team['Task'] in str_cmds:
                    for unit_tuple in team['PT']:
                        self.formation_map[name].add_unit(team['LX'], unit_tuple[0])
        for _, cmds in self.name_cmds_map.items():
            cmds.clear()

    # 所有在编队列表中的编队执行一步的操作
    def make_formation_step(self, formations=None, grand_target_types=None, air_target_types=None):
        if grand_target_types is None:
            grand_target_types = Formation.GRAND_ATTACK_TYPES
        if air_target_types is None:
            air_target_types = Formation.RED_A2A_ATTACK_TYPES
            # air_target_types = [UnitType.AWACS, UnitType.A2A]
        if formations is None:
            formations = [formation for _, formation in self.formation_map.items()]

        cmds = []
        for formation in formations:
            name = formation.name
            if name.startswith('a2a'):
                cmds.extend(formation.step(self.attack_list, air_target_types))
            elif name.startswith('a2g'):
                cmds.extend(formation.step(self.attack_list, grand_target_types))
            else:
                cmds.extend(formation.step())
        return cmds

    # 调整后备编队以及护卫编队
    def make_adjust_formation(self, raw_obs):
        cmds = []
        self.add_unit2formation(raw_obs)
        for airport in raw_obs['airports']:
            a2a_num = airport['AIR']
            a2g_num = airport['BOM']
            for i in range(a2a_num):
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, 1, UnitType.A2A, *A2A_BACK_POINT, *A2A_AREA_PATROL_PARAMS)
                self.name_cmds_map['a2a_back'].append(cmd)
                cmds.append(cmd)  # 起飞歼击机
            for i in range(a2g_num):
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, 1, UnitType.A2G, *A2G_BACK_POINT, *AREA_PATROL_PARAMS)
                self.name_cmds_map['a2g_back'].append(cmd)
                cmds.append(cmd)  # 起飞轰炸机
        self.formation_map['a2a_back'].set_reside_point(*A2A_BACK_POINT)
        self.formation_map['a2g_back'].set_reside_point(*A2G_BACK_POINT)

        a2a_fc_fm = self.formation_map['a2a_fc']
        a2a_fn_fm = self.formation_map['a2a_fn']
        a2a_fs_fm = self.formation_map['a2a_fs']
        a2a_bn_fm = self.formation_map['a2a_bn']
        a2a_bs_fm = self.formation_map['a2a_bs']
        a2a_back_fm = self.formation_map['a2a_back']
        a2g_back_fm = self.formation_map['a2g_back']

        for fm in [a2a_fc_fm, a2a_bn_fm, a2a_bs_fm]:
            while not a2a_back_fm.is_empty() and len(fm.pt_ids[UnitType.A2A]) < A2A_TEAM_SIZE:
                id_ = a2a_back_fm.pop_unit(UnitType.A2A)
                fm.add_unit(UnitType.A2A, id_)
                fm.st = FormationStatus.AREA_PATROL
                cmds.append(EnvCmd.make_areapatrol(id_, *fm.reside_point, *AREA_PATROL_PARAMS))

        if a2a_fc_fm.is_empty():
            for fm in [a2a_fn_fm, a2a_fs_fm]:
                if not fm.is_empty() and fm.is_units_available():
                    self.formation_map['a2a_fc'] = fm
                    self.formation_map[fm.name] = a2a_fc_fm
                    fm.name = 'a2a_fc'
                    fm.st = FormationStatus.AREA_PATROL
                    fm.set_reside_point(*formation_name_reside_point_map['a2a_fc'])
                    return cmds
            for fm in [a2a_bn_fm, a2a_bs_fm]:
                if not fm.is_empty():
                    fm.st = FormationStatus.AREA_PATROL
                    fm.set_reside_point(formation_name_reside_point_map[fm.name][0] - face_en_dis_1,
                                        formation_name_reside_point_map[fm.name][1], 8000)
        return cmds

    def step(self, sim_time, raw_obs):
        cmds = []
        cmds.extend(self.make_step_update(raw_obs))
        print("state:", self.agent_state)
        if self.agent_state > AgentState.FORMATION_STATE:
            cmds.extend(self.make_adjust_formation(raw_obs))

        # TEST
        print("remain:", self.attack_list.remain_plane_num)
        if self.agent_state == AgentState.DEPLOY_STATE:

            # 设置队名和命令的映射表
            self.name_cmds_map['a2a_fn'] = []
            self.name_cmds_map['a2a_fc'] = []
            self.name_cmds_map['a2a_fs'] = []
            self.name_cmds_map['a2a_bn'] = []
            self.name_cmds_map['a2a_bs'] = []
            self.name_cmds_map['awacs'] = []
            self.name_cmds_map['a2a_escort_1'] = []
            self.name_cmds_map['a2a_escort_2'] = []
            self.name_cmds_map['a2a_escort_3'] = []
            self.name_cmds_map['a2g_hunt'] = []
            self.name_cmds_map['a2g_back'] = []  # 用来存储后来从机场补弹后起飞的飞机
            self.name_cmds_map['a2a_back'] = []  # 用来存储后来从机场补弹后起飞的飞机

            map_ = formation_name_reside_point_map
            # 起飞巡航歼击机
            for formation_name in ['a2a_fn', 'a2a_fs', 'a2a_fc', 'a2a_bn', 'a2a_bs']:
                point = map_[formation_name]
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *point, *A2A_AREA_PATROL_PARAMS)
                self.name_cmds_map[formation_name].append(cmd)
                cmds.append(cmd)

            # 设置预警机
            for unit in raw_obs['units']:
                if unit['LX'] == UnitType.AWACS:
                    self.awacs_id = unit['ID']
                    break
            cmd = EnvCmd.make_awcs_areapatrol(self.awacs_id, *AWACS_PATROL_POINT, *AWACS_AREA_PATROL_PARAMS)
            self.name_cmds_map['awacs'].append(cmd)
            cmds.append(cmd)

            # 起飞突击护航歼击机
            for formation_name in ['a2a_escort_1', 'a2a_escort_2', 'a2a_escort_3']:
                point = map_[formation_name]
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *point, *A2A_AREA_PATROL_PARAMS)
                self.name_cmds_map[formation_name].append(cmd)
                cmds.append(cmd)

            for i in range(9):
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *A2G_ASSEMBLY_POINT, *AREA_PATROL_PARAMS)
                self.name_cmds_map['a2g_hunt'].append(cmd)
                cmds.append(cmd)  # 起飞轰炸机

            cmd = EnvCmd.make_takeoff_areapatrol(
                RED_AIRPORT_ID, 1, UnitType.DISTURB, *A2G_ASSEMBLY_POINT, *AREA_PATROL_PARAMS)
            self.name_cmds_map['a2g_hunt'].append(cmd)
            cmds.append(cmd)  # 起飞干扰机

            # 设置舰船
            self.ship_ids = deepcopy(self.unit_ob.ids[self.side][UnitType.SHIP])
            cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[0], *SHIP1_DEPLOY_POINT, 270, 1))
            cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[1], *SHIP2_DEPLOY_POINT, 270, 1))
            cmds.append(EnvCmd.make_ship_areapatrol(self.ship_ids[0], *AWACS_PATROL_POINT, *SHIP_AREA_PATROL_PARAMS))

            # 获取敌方指挥所信息
            for command_id in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
                command_unit = self.unit_ob.id_map[self.en_side][command_id]
                if command_unit.y > 0:
                    self.north_command_id = command_id
                else:
                    self.south_command_id = command_id
            self.agent_state = AgentState.FORMATION_STATE
            return cmds  # 这里要返回，以便于调用下一次step更新raw_obs

        # 将飞机编入对应的编队
        if self.agent_state == AgentState.FORMATION_STATE:
            print("unit_ob", self.unit_ob.ids)
            if sim_time < 20:  # 太早态势里找不到信息
                return cmds
            self.add_unit2formation(raw_obs)  # 将飞机编入编队中

            if len(self.unit_ob.ids[self.side][UnitType.DISTURB]) > 0:
                self.disturb_id = self.unit_ob.ids[self.side][UnitType.DISTURB][0]
            for name, point in formation_name_reside_point_map.items():
                self.formation_map[name].set_reside_point(*point)
            for _, formation in self.formation_map.items():
                formation.print_formation()
            cmds.extend(self.make_formation_step())
            self.agent_state = AgentState.DECISION_STATE
            return cmds

        # 开始决定攻岛
        if self.agent_state == AgentState.DECISION_STATE:
            awacs_fm = self.formation_map['awacs']
            hunt_fm = self.formation_map['a2g_hunt']
            print("fM_mp", self.formation_map)
            for _, fm in self.formation_map.items():
                fm.print_formation()

            # 如果预警机没有到或者轰炸机编队没有到达目标点
            print("到了没", awacs_fm.has_all_arrived(*awacs_fm.reside_point), awacs_fm.compute_2d_center_point())
            if not awacs_fm.has_all_arrived(*awacs_fm.reside_point) or not hunt_fm.st == FormationStatus.AREA_PATROL:
                cmds.extend(self.make_formation_step())
                return cmds
            north_force = 0
            south_force = 0
            for en_id in self.unit_ob.ids[self.en_side][UnitType.A2A]:
                en_unit = self.unit_ob.id_map[self.en_side][en_id]
                if en_unit.y >= 0:
                    north_force += 1
                else:
                    south_force += 1
            for en_id in self.unit_ob.ids[self.en_side][UnitType.SHIP]:
                en_unit = self.unit_ob.id_map[self.en_side][en_id]
                if en_unit.y >= 0:
                    north_force += 4
                else:
                    south_force += 4
            self.firstly_attack_target = 'south' if south_force <= north_force + 2 else 'north'  # 南岛好打一些，补充一点指数
            print("决定了我们就打：", self.firstly_attack_target)

            # self.firstly_attack_target = 'north'  # 需要修改

            self.task_formations = []
            if self.firstly_attack_target == 'south':

                self.agent_state = AgentState.SOUTH_ATTACK_STATE
                for formation_name, point in formation_name_reside_point_map_south_assembly.items():
                    self.formation_map[formation_name].set_reside_point(*point)
                    self.task_formations.append(self.formation_map[formation_name])
            else:
                self.agent_state = AgentState.NORTH_ATTACK_STATE  # 待更新
                for formation_name, point in formation_name_reside_point_map_north_assembly.items():
                    self.formation_map[formation_name].set_reside_point(*point)
                    self.task_formations.append(self.formation_map[formation_name])

            for formation in self.task_formations:
                formation.st = FormationStatus.AREA_PATROL

            cmds.extend(self.make_formation_step())
            return cmds

        # 攻打南岛步骤：
        if self.agent_state // 10 == AgentState.SOUTH_ATTACK_STATE // 10:
            formations_ = []
            for name, _ in self.formation_map.items():
                if name not in ['a2g_hunt', 'a2a_escort_1', 'a2a_escort_2', 'a2a_escort_3']:
                    formations_.append(self.formation_map[name])
            cmds.extend(self.make_formation_step(formations_))

            hunt_fm = self.formation_map.get('a2g_hunt')

            if hunt_fm.is_empty() or self.south_command_id == 0:
                self.retreat_line = [[-216929, -254155, 8000], SOUTH_A2G_ASSEMBLY_POINT, A2G_ASSEMBLY_POINT]
                for formation in self.task_formations:
                    formation.st = FormationStatus.AREA_PATROL
                    formation.set_reside_point(*self.retreat_line[0])
                self.agent_state = AgentState.RETREAT_STATE
                cmds.extend(self.make_formation_step(self.task_formations))
                return cmds

            # 判断当前子阶段
            # 在集合点需要全部飞机到齐，在突击点只需要轰炸机到齐
            map_ = formation_name_reside_point_map_south_assembly
            map__ = formation_name_reside_point_map_south_hunt
            flag_ = True
            # for fm in self.task_formations:
            for fm in [hunt_fm]:
                if not (fm.is_empty() or (
                        fm.st in [FormationStatus.AREA_PATROL, FormationStatus.CIRCLING] and fm.reside_point == map_[
                    fm.name])):
                    flag_ = False

            if flag_:
                for fm in self.task_formations:
                    fm.set_reside_point(*map__[fm.name])
                self.agent_state = AgentState.SOUTH_ATTACK_STATE_PARSE1
                cmds.extend(self.make_formation_step(self.task_formations))
                return cmds

            if hunt_fm.st == FormationStatus.AREA_PATROL and hunt_fm.reside_point == map__['a2g_hunt']:
                hunt_fm.set_reside_point(*BLUE_SOUTH_COMMANDER_POINT)
                self.agent_state = AgentState.SOUTH_ATTACK_STATE_PARSE2
                cmds.extend(self.make_formation_step(self.task_formations))
                return cmds

            cmds.extend(self.make_formation_step(self.task_formations, [UnitType.COMMAND, UnitType.SHIP]))
            return cmds

        elif self.agent_state == AgentState.RETREAT_STATE:
            formations_ = []
            for name, _ in self.formation_map.items():
                if name not in ['a2g_hunt', 'a2a_escort_1', 'a2a_escort_2', 'a2a_escort_3']:
                    formations_.append(self.formation_map[name])
            cmds.extend(self.make_formation_step(formations_))
            hunt_fm = self.formation_map['a2g_hunt']
            print("retreat state: hunt_fm st , reside_point", hunt_fm.st, hunt_fm.reside_point)
            for i in range(len(self.retreat_line)):
                if hunt_fm.st == FormationStatus.AREA_PATROL and hunt_fm.reside_point == self.retreat_line[i]:
                    if i == len(self.retreat_line) - 1:
                        self.agent_state = AgentState.WAITING_FOR_REINFORCEMENT
                        cmds.extend(self.make_formation_step(self.task_formations))
                        return cmds
                    else:
                        for fm in self.task_formations:
                            fm.print_formation()
                            fm.set_reside_point(*self.retreat_line[i + 1])
                            fm.st = FormationStatus.AREA_PATROL

                        cmds.extend(self.make_formation_step(self.task_formations))
                        return cmds
            cmds.extend(self.make_formation_step(self.task_formations))
            return cmds

        elif self.agent_state == AgentState.WAITING_FOR_REINFORCEMENT:
            cmds.extend(self.make_formation_step())
            a2a_fc_fm = self.formation_map['a2a_fc']
            a2a_fn_fm = self.formation_map['a2a_fn']
            a2a_fs_fm = self.formation_map['a2a_fs']
            a2a_bn_fm = self.formation_map['a2a_bn']
            a2a_bs_fm = self.formation_map['a2a_bs']
            a2a_back_fm = self.formation_map['a2a_back']
            a2g_back_fm = self.formation_map['a2g_back']
            hunt_fm = self.formation_map['a2g_hunt']
            escort_fm_1 = self.formation_map.get('a2a_escort_1')
            escort_fm_2 = self.formation_map.get('a2a_escort_2')
            escort_fm_3 = self.formation_map.get('a2a_escort_3')
            awacs_fm = self.formation_map.get('awacs')

            while not a2g_back_fm.is_empty():
                id_ = a2g_back_fm.pop_unit(UnitType.A2G)
                hunt_fm.add_unit(UnitType.A2G, id_)
                hunt_fm.st = FormationStatus.AREA_PATROL
                cmds.append(EnvCmd.make_areapatrol(id_, *hunt_fm.reside_point, *AREA_PATROL_PARAMS))

            if sim_time > 6300:
                while not a2a_fc_fm.is_empty():
                    id_ = a2a_fc_fm.pop_unit(UnitType.A2A)
                    escort_fm_1.add_unit(UnitType.A2A, id_)
                    escort_fm_1.st = FormationStatus.AREA_PATROL
                    cmds.append(EnvCmd.make_areapatrol(id_, *escort_fm_1.reside_point, *AREA_PATROL_PARAMS))

                while not a2a_fs_fm.is_empty():
                    id_ = a2a_fs_fm.pop_unit(UnitType.A2A)
                    escort_fm_2.add_unit(UnitType.A2A, id_)
                    escort_fm_2.st = FormationStatus.AREA_PATROL
                    cmds.append(EnvCmd.make_areapatrol(id_, *escort_fm_2.reside_point, *AREA_PATROL_PARAMS))

                while not a2a_fn_fm.is_empty():
                    id_ = a2a_fn_fm.pop_unit(UnitType.A2A)
                    escort_fm_3.add_unit(UnitType.A2A, id_)
                    escort_fm_3.st = FormationStatus.AREA_PATROL
                    cmds.append(EnvCmd.make_areapatrol(id_, *escort_fm_3.reside_point, *AREA_PATROL_PARAMS))

                while not a2a_back_fm.is_empty():
                    id_ = a2a_back_fm.pop_unit(UnitType.A2A)
                    escort_fm_2.add_unit(UnitType.A2A, id_)
                    escort_fm_2.st = FormationStatus.AREA_PATROL
                    cmds.append(EnvCmd.make_areapatrol(id_, *escort_fm_2.reside_point, *AREA_PATROL_PARAMS))

                while not awacs_fm.is_empty():
                    id_ = awacs_fm.pop_unit(UnitType.AWACS)
                    hunt_fm.add_unit(UnitType.AWACS, id_)
                    hunt_fm.st = FormationStatus.AREA_PATROL
                    cmds.append(EnvCmd.make_areapatrol(id_, *hunt_fm.reside_point, *AREA_PATROL_PARAMS))

                # 有大量冗余代码
                self.task_formations = []
                if self.firstly_attack_target == 'south' and self.south_command_id != 0 or \
                        self.firstly_attack_target == 'north' and self.north_command_id == 0:
                    self.agent_state = AgentState.SOUTH_ATTACK_STATE
                    for formation_name, point in formation_name_reside_point_map_south_assembly.items():
                        self.formation_map[formation_name].set_reside_point(*point)
                        self.task_formations.append(self.formation_map[formation_name])
                else:
                    self.agent_state = AgentState.NORTH_ATTACK_STATE
                    for formation_name, point in formation_name_reside_point_map_north_assembly.items():
                        self.formation_map[formation_name].set_reside_point(*point)
                        self.task_formations.append(self.formation_map[formation_name])

                for formation in self.task_formations:
                    formation.st = FormationStatus.AREA_PATROL

            cmds.extend(self.make_formation_step())
            return cmds

        if self.agent_state // 10 == AgentState.NORTH_ATTACK_STATE // 10:
            formations_ = []
            for name, _ in self.formation_map.items():
                if name not in ['a2g_hunt', 'a2a_escort_1', 'a2a_escort_2', 'a2a_escort_3']:
                    formations_.append(self.formation_map[name])
            cmds.extend(self.make_formation_step(formations_))

            hunt_fm = self.formation_map.get('a2g_hunt')

            if hunt_fm.is_empty() or self.north_command_id == 0:
                self.retreat_line = [NORTH_A2G_ASSEMBLY_POINT, A2G_ASSEMBLY_POINT]
                for formation in self.task_formations:
                    formation.st = FormationStatus.AREA_PATROL
                    formation.set_reside_point(*self.retreat_line[0])
                self.agent_state = AgentState.RETREAT_STATE
                cmds.extend(self.make_formation_step(self.task_formations))
                return cmds

            # 判断当前子阶段
            # 在集合点需要全部飞机到齐，在突击点只需要轰炸机到齐
            map_ = formation_name_reside_point_map_north_assembly
            map__ = formation_name_reside_point_map_north_hunt
            flag_ = True
            # for fm in self.task_formations:
            for fm in [hunt_fm]:
                if not (fm.is_empty() or
                        (fm.st in [FormationStatus.AREA_PATROL, FormationStatus.CIRCLING] and
                         fm.reside_point == map_[fm.name])):
                    flag_ = False

            if flag_:
                for fm in self.task_formations:
                    fm.set_reside_point(*map__[fm.name])
                self.agent_state = AgentState.NORTH_ATTACK_STATE_PARSE1
                cmds.extend(self.make_formation_step(self.task_formations))
                return cmds

            # 可以改进ob地防
            cmds.extend(self.make_formation_step(self.task_formations, [UnitType.COMMAND, UnitType.SHIP, UnitType.S2A]))
            return cmds

        else:
            print("来到了else")
            print(cmds)

        return cmds
