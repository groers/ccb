from env.env_def import UnitType

SHIP_AREA_PATROL_PARAMS = [270, 1, 1, 20, 10800]
AREA_PATROL_PARAMS = [270, 10000, 10000, 250, 10800]
A2A_AREA_PATROL_PARAMS = [270, 10000, 10000, 300, 10800]
AWACS_AREA_PATROL_PARAMS = [270, 1, 1, 250, 10800, 0]  # 预警机最大速度为500

RED_AIRPORT_POINT = [251004, 25526, 8000]
BLUE_SOUTH_COMMANDER_POINT = [-295519, -100815, 8000]
BLUE_NORTH_COMMANDER_POINT = [-250830, 184289, 8000]

# SHIP1_DEPLOY_POINT = [125000, -35864, 8000]
SHIP1_DEPLOY_POINT = [125000, -250000, 8000]
SHIP2_DEPLOY_POINT = [241004, 25526, 8000]

# ASSEMBLY_POINT_STATE_1 = [50563, -35864, 8000]

face_en_dis_1 = 60000  # 迎敌方向编队间距
vert_en_dis_1 = 100000  # 垂直地方方向编队间距

AWACS_PATROL_POINT = [50564, -35864, 8000]  # 向前压一点，不然识别距离不够
_p = AWACS_PATROL_POINT
A2A_FN_PATROL_POINT = [_p[0] - face_en_dis_1, _p[1] + 2 * vert_en_dis_1, 8000]  # front north
A2A_FC_PATROL_POINT = [_p[0] - face_en_dis_1, _p[1], 8000]  # front center
A2A_FS_PATROL_POINT = [_p[0] - face_en_dis_1, _p[1] - 2 * vert_en_dis_1, 8000]  # front south
A2A_BN_PATROL_POINT = [_p[0], _p[1] + vert_en_dis_1, 8000]  # back north
A2A_BS_PATROL_POINT = [_p[0], _p[1] - vert_en_dis_1, 8000]  # back south

A2G_ASSEMBLY_POINT = [_p[0] + face_en_dis_1, _p[1], 8000]  # DEPLOY阶段轰炸机集合点

A2G_BACK_POINT = [_p[0] + face_en_dis_1 - 4, _p[1], 8000]  # 后备轰炸机集合点
A2A_BACK_POINT = [_p[0] + face_en_dis_1 - 3, _p[1], 8000]  # 后备歼击机集合点

face_en_dis_2 = 20000  # 迎敌方向编队间距
vert_en_dis_2 = vert_en_dis_1 // 2  # 垂直地方方向编队间距
# 设置x坐标不同，以识别出不是一个编队
ESCORT_1_ASSEMBLY_POINT = [_p[0] + face_en_dis_1 - vert_en_dis_2, _p[1], 8000]
ESCORT_2_ASSEMBLY_POINT = [_p[0] + face_en_dis_1 - 2, _p[1], 8000]
ESCORT_3_ASSEMBLY_POINT = [_p[0] + face_en_dis_1 - 3, _p[1], 8000]

# 南方集合阶段
SOUTH_A2G_ASSEMBLY_POINT = [-1936, -254155, 8000]  # y原来的值-230225
_p = SOUTH_A2G_ASSEMBLY_POINT
SOUTH_A2A_L_ASSEMBLY_POINT = [_p[0] - vert_en_dis_2 * 2, _p[1] + face_en_dis_2, 8000]
SOUTH_A2A_C_ASSEMBLY_POINT = [_p[0] - vert_en_dis_2, _p[1] + face_en_dis_2, 8000]
SOUTH_A2A_R_ASSEMBLY_POINT = [_p[0], _p[1] + face_en_dis_2, 8000]

# 突击南岛阶段
SOUTH_A2G_HUNT_POINT = [-266719, -254155, 8000]
_p = SOUTH_A2G_HUNT_POINT
SOUTH_A2A_L_HUNT_POINT = [_p[0], _p[1] + face_en_dis_2, 8000]
SOUTH_A2A_C_HUNT_POINT = [_p[0] + vert_en_dis_2, _p[1] + face_en_dis_2, 8000]
SOUTH_A2A_R_HUNT_POINT = [_p[0] + vert_en_dis_2 * 2, _p[1] + face_en_dis_2, 8000]

# 北方集合阶段
NORTH_A2G_ASSEMBLY_POINT = [-6046, 205736, 8000]
_p = NORTH_A2G_ASSEMBLY_POINT
NORTH_A2A_L_ASSEMBLY_POINT = [_p[0] - vert_en_dis_2 * 2, _p[1] - face_en_dis_2, 8000]
NORTH_A2A_C_ASSEMBLY_POINT = [_p[0] - vert_en_dis_2, _p[1] - face_en_dis_2, 8000]
NORTH_A2A_R_ASSEMBLY_POINT = [_p[0], _p[1] - face_en_dis_2, 8000]


# 突击北岛阶段
NORTH_A2G_HUNT_POINT = BLUE_NORTH_COMMANDER_POINT
_p = NORTH_A2G_HUNT_POINT
NORTH_A2A_L_HUNT_POINT = [_p[0], _p[1] - face_en_dis_2, 8000]
NORTH_A2A_C_HUNT_POINT = [_p[0] + vert_en_dis_2, _p[1] - face_en_dis_2, 8000]
NORTH_A2A_R_HUNT_POINT = [_p[0] + vert_en_dis_2 * 2, _p[1] - face_en_dis_2, 8000]


A2G_TEAM_SIZE = 2  # 单个轰炸机编队含轰炸机个数
A2A_TEAM_SIZE = 3  # 单个歼击机编队含歼击机个数

PLANE_TYPES = [UnitType.A2A, UnitType.A2G, UnitType.AWACS, UnitType.DISTURB]

formation_name_reside_point_map = {
    'a2a_fn': A2A_FN_PATROL_POINT,
    'a2a_fc': A2A_FC_PATROL_POINT,
    'a2a_fs': A2A_FS_PATROL_POINT,
    'a2a_bn': A2A_BN_PATROL_POINT,
    'a2a_bs': A2A_BS_PATROL_POINT,
    'awacs': AWACS_PATROL_POINT,
    'a2a_escort_1': ESCORT_1_ASSEMBLY_POINT,
    'a2a_escort_2': ESCORT_2_ASSEMBLY_POINT,
    'a2a_escort_3': ESCORT_3_ASSEMBLY_POINT,
    'a2g_hunt': A2G_ASSEMBLY_POINT,
}

formation_name_reside_point_map_south_assembly = {
    'a2a_escort_1': SOUTH_A2A_L_ASSEMBLY_POINT,
    'a2a_escort_2': SOUTH_A2A_C_ASSEMBLY_POINT,
    'a2a_escort_3': SOUTH_A2A_R_ASSEMBLY_POINT,
    'a2g_hunt': SOUTH_A2G_ASSEMBLY_POINT,
}

formation_name_reside_point_map_south_hunt = {
    'a2a_escort_1': SOUTH_A2A_L_HUNT_POINT,
    'a2a_escort_2': SOUTH_A2A_C_HUNT_POINT,
    'a2a_escort_3': SOUTH_A2A_R_HUNT_POINT,
    'a2g_hunt': SOUTH_A2G_HUNT_POINT,
}

formation_name_reside_point_map_north_assembly = {
    'a2a_escort_1': NORTH_A2A_L_ASSEMBLY_POINT,
    'a2a_escort_2': NORTH_A2A_C_ASSEMBLY_POINT,
    'a2a_escort_3': NORTH_A2A_R_ASSEMBLY_POINT,
    'a2g_hunt': NORTH_A2G_ASSEMBLY_POINT,
}

formation_name_reside_point_map_north_hunt = {
    'a2a_escort_1': NORTH_A2A_L_HUNT_POINT,
    'a2a_escort_2': NORTH_A2A_C_HUNT_POINT,
    'a2a_escort_3': NORTH_A2A_R_HUNT_POINT,
    'a2g_hunt': NORTH_A2G_HUNT_POINT,
}