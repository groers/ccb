from agent.NJU.bp_env import EnvCmd
from agent.NJU.common.interface.base_rule import BaseRulePlayer
from agent.NJU.bp_env.env_def import UnitType, BLUE_AIRPORT_ID, SideType, RED_AIRPORT_ID, TeamStatus
from agent.NJU.common.cmd import Command
from agent.NJU.common.interface.task import Task, TaskState
from agent.NJU.common.units.a2a import A2A
import numpy as np
from agent.NJU.drill.api.bp.gear.player import Player

from agent.NJU.player import unit_ob
from agent.NJU.player.attack_list import AttackList
from agent.NJU.player.formation import Formation
from agent.NJU.player.unit_ob import UnitOb

from agent.agent import Agent

#from common.units.ship import SHIP

TAKEOFF_PATROL_POINT_1 = [-100000, 0, 8000]
TAKEOFF_PATROL_POINT_2 = [-100000, 125000, 8000]
TAKEOFF_PATROL_POINT_3 = [-100000, -125000, 8000]
TAKEOFF_PATROL_POINT_4 = [-160000, 125000, 8000]
TAKEOFF_PATROL_POINT_5 = [-160000, -125000, 8000]

AREA_PATROL_PARAMS = [270, 10000, 10000, 250, 10800]
A2A_AREA_PATROL_PARAMS = [270, 10000, 10000, 300, 10800]
A2A_AREA_PATROL_PARAMS = [270, 10000, 10000, 1300, 10800]
SHIP_AREA_PATROL_PARAMS = [270, 1, 1, 20, 10800]

'''
    主要兵力守护北岛
'''

SHIP1_DEPLOY_POINT = [-125000, 30000, 0]
SHIP2_DEPLOY_POINT = [-150000, 0, 0]
# guyuhao TEST
# SHIP1_DEPLOY_POINT = [-125000, -250000, 0]
# SHIP2_DEPLOY_POINT = [-150000, -250000, 0]


SHIP_PATROL_POINT = [-240000, 125000, 0]

A2G_TEAM_SIZE = 2  # 单个轰炸机编队含轰炸机个数
A2A_TEAM_SIZE = 3  # 单个歼击机编队含歼击机个数

BOM_READY = 0


class AgentState:
    DEPLOY_STATE = 10  # 部署单位，起飞单位
    FORMATION_STATE = 20  # 编队阶段
    COMBAT_STATE = 30  # 对战阶段
    SAVE_STATE = 40  # 主守北岛阶段


class A2ASupportTask(Task):
    def __init__(self, task_state):
        super().__init__(task_state)
        self.done_unit_ids = []

    def _attack(self, obs):
        cmds = []
        unit_ids = []
        en_unit_ids = []
        for _, unit in self.units_map.items():
            for en_unit in obs['qb']:
                if en_unit['LX'] in [11, 15]:
                    dist = unit.compute_2d_distance(en_unit['X'], en_unit['Y'])
                    if dist < 120000:
                        # cmds.append(Command.a2a_attack(unit.id, en_unit['ID']))
                        unit_ids.append(unit.id)
                        en_unit_ids.append(en_unit['ID'])
        unit_ids = list(set(unit_ids))
        en_unit_ids = list(set(en_unit_ids))
        for unit_id in unit_ids:
            np.random.shuffle(en_unit_ids)
            for en_unit_id in en_unit_ids:
                cmds.append(Command.a2a_attack(unit_id, en_unit_id))
        return cmds


class A2ASupportPoint(A2ASupportTask):
    def __init__(self, max_unit_num, target_point, task_state=TaskState.PREPARE):
        super().__init__(task_state)
        self.max_unit_num = max_unit_num
        self.target_point = target_point

    def get_all_missiles(self):
        missile_num = 0
        for _, unit in self.units_map.items():
            missile_num += unit.get_missile_num()
        return missile_num

    def update_task_state(self):
        pass

    def update(self, alive_unit_map):
        self.update_units_map(alive_unit_map)

    def update_ship(self, alive_unit_map):
        self.update_ship_map(alive_unit_map)

    def run(self, idle_unit_map, obs):
        cmds = []
        attack_cmds = self._attack(obs)
        cmds.extend(attack_cmds)
        if self.task_state != TaskState.FINISH:
            while len(self.units_map) < self.max_unit_num:
                if len(idle_unit_map) == 0:
                    break
                unit = idle_unit_map.popitem()[1]
                self.add_unit(unit)
            if len(attack_cmds) == 0:
                for unit_id, _ in self.units_map.items():
                    cmds.append(Command.area_patrol(unit_id, [self.target_point[0], self.target_point[1], 8000]))
        else:
            for unit_id, _ in self.units_map.items():
                cmds.append(Command.area_patrol(unit_id, TAKEOFF_PATROL_POINT))
            self.finish()
        return cmds


TYPE2STRING_MAP = {11: "AIR", 12: "AWCS", 13: "JAM", 15: "BOM"}


class BluePlayer(Agent):
    AWACS_ESCORT_TEAM_NUM = 2

    name_cmds_map = {}  # key为编队名，value为编队包含的单位所执行过的命令

    def __init__(self, name, config):
        super().__init__(name, config['side'])
        self.side = config['side']
        self.en_side = 'red' if config['side'] == 'blue' else 'blue'
        self.attack_list = AttackList(self.side)
        self.unit_ob = UnitOb(config['side'])
        self.formation_map = {}  # key为编队名，value为编队
        self.a2a_task = A2ASupportPoint(3, [-30000, 0])  # 以少打大：每次都用三架飞机进行打击
        self.agent_state = AgentState.DEPLOY_STATE
        self.ship_ids = [0, 0]
        self.awacs_id = 0

        # 将单位分进编队里

    def add_unit2formation(self, raw_obs):
        for name, cmds in self.name_cmds_map.items():
            if name not in self.formation_map:
                self.formation_map[name] = Formation(self.side, name, TeamStatus.AREA_PATROL, self.unit_ob)
            str_cmd = [str(cmd).replace("\'", "\"") for cmd in cmds]  # team['Task']其实是个字符串，要比较的话，需要对cmd做str
            for team in raw_obs['teams']:
                if team['Task'] in str_cmd:
                    for unit_tuple in team['PT']:
                        self.formation_map[name].add_unit(team['LX'], unit_tuple[0])

    def _get_waiting_aircraft_num(self, raw_obs, type_):
        if type_ not in TYPE2STRING_MAP.keys():
            return 0
        type_str = TYPE2STRING_MAP[type_]
        return raw_obs['airports'][0][type_str]

    def _take_off(self, raw_obs):
        cmds = []
        fly_types = [UnitType.A2A, UnitType.A2G, UnitType.JAM]

        a = "轰炸机"
        # print(a)
        # 起飞轰炸机
        for i in range(5):
            if i == 0:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *TAKEOFF_PATROL_POINT_1, *AREA_PATROL_PARAMS)
            elif i == 1:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *TAKEOFF_PATROL_POINT_2, *AREA_PATROL_PARAMS)
            elif i == 2:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *TAKEOFF_PATROL_POINT_3, *AREA_PATROL_PARAMS)
            elif i == 3:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *TAKEOFF_PATROL_POINT_4, *AREA_PATROL_PARAMS)
            else:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *TAKEOFF_PATROL_POINT_5, *AREA_PATROL_PARAMS)
            self.name_cmds_map['hunt_formation'].append(cmd)
            cmds.append(cmd)

        a = "歼击机"
        # print(a)
        # 起飞歼击机（15架）  还有五架保护预警机
        for i in range(5):
            if i == 0:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *TAKEOFF_PATROL_POINT_1, *AREA_PATROL_PARAMS)
            elif i == 1:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *TAKEOFF_PATROL_POINT_2, *AREA_PATROL_PARAMS)
            elif i == 2:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *TAKEOFF_PATROL_POINT_3, *AREA_PATROL_PARAMS)
            elif i == 3:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *TAKEOFF_PATROL_POINT_4, *AREA_PATROL_PARAMS)
            else:
                cmd = EnvCmd.make_takeoff_areapatrol(
                    BLUE_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *TAKEOFF_PATROL_POINT_5, *AREA_PATROL_PARAMS)

            self.name_cmds_map['hunt_formation'].append(cmd)
            cmds.append(cmd)  # 起飞歼击机

        return cmds

    def _awacs_task(self, raw_obs):  # 预警机巡逻区域
        cmds = []
        patrol_points = [-160000, 0, 8000]
        # TODO(zhoufan): 是否应该将awacs的id缓存起来
        for unit in raw_obs['units']:
            if unit['LX'] == UnitType.AWACS:
                self.awacs_id = unit['ID']
                cmds.append(
                    Command.awacs_areapatrol(
                        unit['ID'], patrol_points))
                break

        a = "歼击机护航预警机"
        # print(a)
        # 歼击机护航预警机
        for i in range(1, 4):
            cmd = EnvCmd.make_takeoff_protect(BLUE_AIRPORT_ID, self.AWACS_ESCORT_TEAM_NUM, self.awacs_id, i, 5, 100)
            self.name_cmds_map['awacs_formation'].append(cmd)
            cmds.append(cmd)
        return cmds

    def _ship(self, raw_obs):  # 舰船
        cmds = []
        self.ship_ids = self.unit_ob.ids[self.side][UnitType.SHIP]

        a = "舰船部署"
        # print(a)
        cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[0], *SHIP1_DEPLOY_POINT, 270, 1))
        cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[1], *SHIP2_DEPLOY_POINT, 270, 1))
        '''
        jam = self.unit_ob.ids[SideType.RED][UnitType.JAM]
        awacs = self.unit_ob.ids[SideType.RED][UnitType.AWACS]
        if len(jam) > 0:
            cmds.extend([EnvCmd.make_ship_addtarget(self.ship_ids[0], jam[0])])
            cmds.extend([EnvCmd.make_ship_addtarget(self.ship_ids[1], jam[0])])
        if len(awacs) > 0:
            cmds.extend([EnvCmd.make_ship_addtarget(self.ship_ids[1], awacs[0])])
        '''
        return cmds

    def _get_units_map(self, raw_obs, type_):
        units_map = {}
        for info_map in raw_obs['units']:
            if info_map['LX'] == type_:
                units_map[info_map['ID']] = A2A(info_map)
        return units_map

    def _get_ship_map(self, raw_obs, type_):
        units_map = {}
        for info_map in raw_obs['units']:
            if info_map['LX'] == type_:
                units_map[info_map['ID']] = SHIP(info_map)
        return units_map

    def _find_Bom_Ready(self, raw_obs):
        global BOM_READY
        for info_map in raw_obs['units']:
            if info_map['LX'] == UnitType.A2G:
                if info_map['Y'] > 200000:
                    BOM_READY = 1

    def _Bom_Attack(self, raw_obs):
        cmds = []
        boms = self.unit_ob.ids[SideType.RED][UnitType.A2G]
        for unit in boms:
            # print(unit)
            cmds.append(
                Command.target_hunt(unit, RED_AIRPORT_ID, 60, direction=90))
        return cmds

    def step_update(self, raw_obs):
        cmds = []
        self.unit_ob.update(raw_obs)  # 更新态势
        cmds_, return_ids = self.attack_list.make_update(self.unit_ob)  # 新增一个返回值，返回返航集合
        cmds.extend(cmds_)
        for _, formation in self.formation_map.items():
            formation.update(self.attack_list, self.unit_ob, return_ids)
        '''
        if self.disturb_id not in self.unit_ob.ids[self.side][UnitType.DISTURB]:
            self.disturb_id = 0
        if self.awacs_id not in self.unit_ob.ids[self.side][UnitType.AWACS]:
            self.awacs_id = 0
        if self.south_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.south_command_id = 0
        if self.north_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.north_command_id = 0
        '''
        return cmds

    def formation_patrol_attack(self, grand_target_types=None, air_target_types=None):
        if grand_target_types is None:
            grand_target_types = Formation.GRAND_ATTACK_TYPES
        if air_target_types is None:
            air_target_types = Formation.BLUE_A2A_ATTACK_TYPES
        cmds = []
        for name, formation in self.formation_map.items():
            cmd = formation.make_patrol_attack(self.attack_list, grand_target_types, air_target_types)
            # print(name, "attack_cmd", cmd)
            cmds.extend(cmd)
        return cmds

    def _biandui(self, raw_obs):
        cmds = []
        # 将飞机编入对应的编队
        if self.agent_state == AgentState.FORMATION_STATE:
            self.add_unit2formation(raw_obs)  # 将飞机编入编队中
            self.name_cmds_map.clear()
            # for _, formation in self.formation_map.items():
            #     formation.print_formation()
            cmds.extend(self.formation_patrol_attack())
            self.agent_state = AgentState.COMBAT_STATE

        return cmds

    def _ship_save(self, raw_obs):
        cmds = []
        cmds.append(EnvCmd.make_ship_areapatrol(self.ship_ids[1], *SHIP_PATROL_POINT, *SHIP_AREA_PATROL_PARAMS))
        return cmds

    def _fly_save(self, raw_obs):
        cmds = []
        fly_types = [UnitType.A2A, UnitType.A2G, UnitType.JAM]
        for type_ in fly_types:
            if self._get_waiting_aircraft_num(raw_obs, type_):
                cmds.append(
                    Command.takeoff_areapatrol(
                        BLUE_AIRPORT_ID, 1, type_, patrol_points=TAKEOFF_PATROL_POINT))
        return cmds
        return cmds

    def step(self, sim_time, raw_obs):
        cmds = []
        cmds.extend(self.step_update(raw_obs))

        if self.agent_state == AgentState.DEPLOY_STATE:
            self.name_cmds_map['awacs_formation'] = []
            self.name_cmds_map['hunt_formation'] = []

            a = "作战飞机"
            # print(a)
            cmds.extend(self._take_off(raw_obs))  # 起飞作战飞机
            a = "预警机"
            # print(a)
            cmds.extend(self._awacs_task(raw_obs))  # 起飞预警机
            a = "舰船"
            # print(a)
            cmds.extend(self._ship(raw_obs))  # 舰船

            self.agent_state = AgentState.FORMATION_STATE
            return cmds # 这里要返回，以便于调用下一次step更新raw_obs

        if self.agent_state == AgentState.FORMATION_STATE:
            cmds.extend(self._biandui(raw_obs))
            a = "编队完成"
            # print(a)
            #self.agent_state = AgentState.COMBAT_STATE
            # return cmds  # 这里要返回，以便于调用下一次step更新raw_obs

        a = "开始攻击"
        # print(a)
        if self.agent_state == AgentState.COMBAT_STATE:
            a = "攻击"
            # print(a)
            a2a_map = self._get_units_map(raw_obs, UnitType.A2A)
            # 将挂掉的飞机剔除
            self.a2a_task.update(a2a_map)
            for unit_id, _ in self.a2a_task.get_units_map().items():
                a2a_map.pop(unit_id)
            # 余下作战飞机执行任务
            cmds.extend(self.a2a_task.run(a2a_map, raw_obs))
        '''
        # 判断是否需要回去守主要岛屿
        north_force = 0
        south_force = 0
        for en_id in self.unit_ob.ids[self.en_side][UnitType.A2A]:
            en_unit = self.unit_ob.id_map[self.en_side][en_id]
            if en_unit.y >= 0:
                north_force += 1
            else:
                south_force += 1

        if north_force > south_force:
            self.agent_state = AgentState.SAVE_STATE

        a = "开始主防北岛"
        # print(a)
        if self.agent_state == AgentState.COMBAT_STATE:
            cmds.extend(self._ship_save(raw_obs))
            cmds.extend(self._fly_save(raw_obs))
        '''
        return cmds

    def reset(self, raw_obs, env_step_info):
        pass
