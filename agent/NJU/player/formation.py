from env.env_def import UnitType, RED_AIRPORT_ID, BLUE_AIRPORT_ID
from env.env_cmd import EnvCmd
from copy import deepcopy
from agent.NJU.player.utils import compute_2d_distance_point, will_attack_by_ship
import numpy as np

A2G_DISTANCE = 115000
A2A_DISTANCE = 100000

AREA_PATROL_PARAMS = [270, 10000, 10000, 250, 10800]


class FormationStatus:
    READY = 0  # 可发送指令
    AREA_PATROL = 6  # 在驻扎点区域巡逻
    CIRCLING = 7  # 原地盘旋
    MOVING_2_POINT = 40  # 前往驻扎点巡逻的路上
    TARGET_HUNT = 12  # 目标攻击状态
    SHIP_HUNT = 13  # 突击路径上的舰船


# 编队类
class Formation:
    RED_A2A_ATTACK_TYPES = [UnitType.AWACS, UnitType.A2A, UnitType.A2G]  # 红方对空打击优先级列表
    BLUE_A2A_ATTACK_TYPES = [UnitType.AWACS, UnitType.DISTURB, UnitType.A2G, UnitType.A2A]  # 蓝方对空打击优先级列表
    GRAND_ATTACK_TYPES = [UnitType.SHIP, UnitType.S2A]  # 对地打击优先级列表
    NEED_ATTACK_NUM = {  # 打击每种平台需要的飞机数目
        UnitType.A2A: 2, UnitType.A2G: 1, UnitType.AWACS: 2, UnitType.DISTURB: 1, UnitType.SHIP: 4,
        UnitType.S2A: 2, UnitType.RADAR: 1, UnitType.COMMAND: 4, UnitType.AIRPORT: 1,
    }

    def __init__(self, side, name, st, unit_ob):
        self.side = side
        self.en_side = 'blue' if side == 'red' else 'red'
        self.name = name
        self.st = st  # 编队状态
        self.unit_ob = unit_ob
        self.reside_point = [0, 0, 0]  # 编队巡逻驻扎点
        self.pt_ids = {UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: []}
        self.available_unit_num = {UnitType.A2A: 0, UnitType.A2G: 0, UnitType.AWACS: 0, UnitType.DISTURB: 0}
        self.available_unit_ids = {UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: []}

    def update(self, attack_list, unit_ob, return_ids):
        cmds = []
        self.unit_ob = unit_ob
        self.available_unit_num = {UnitType.A2A: 0, UnitType.A2G: 0, UnitType.AWACS: 0, UnitType.DISTURB: 0}
        self.available_unit_ids = {UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: []}
        for type_ in [UnitType.A2A, UnitType.A2G, UnitType.AWACS, UnitType.DISTURB]:
            rm_list = []
            for id_ in self.pt_ids[type_]:
                unit = unit_ob.id_map[self.side].get(id_)
                if unit is None:  # 数目不对
                    attack_list.remain_plane_num[type_] -= 1
                # 修复编队中弹药为0的单位不返航的问题,可以修改attack_list的内容了
                if unit is not None and unit.unit_type in [UnitType.A2A, UnitType.A2G]:
                    missile_num = 0
                    for _, v in unit.info_map['WP'].items():
                        missile_num = v
                        break
                    if missile_num == 0:  # 导弹量为0则返航
                        rm_list.append(id_)
                        if unit.unit_type in [UnitType.A2A, UnitType.A2G]:
                            cmds.append(
                                EnvCmd.make_returntobase(id_, RED_AIRPORT_ID if self.side == 'red' else BLUE_AIRPORT_ID)
                            )
                        break
                if unit is not None and id_ not in return_ids:  # 如果单位存活且不在返航集合中
                    if id_ not in attack_list.task_ids:
                        self.available_unit_num[type_] += 1
                        self.available_unit_ids[type_].append(id_)
                else:
                    rm_list.append(id_)

            for id_ in rm_list:
                self.pt_ids[type_].remove(id_)
        return cmds

    def set_reside_point(self, x, y, z):
        self.reside_point = [x, y, z]

    def add_unit(self, type_, id_):
        self.pt_ids[type_].append(id_)
        self.available_unit_num[type_] += 1
        self.available_unit_ids[type_].append(id_)

    def del_unit(self, type_, id_):
        self.pt_ids[type_].remove(id_)
        if id_ in self.available_unit_ids[type_]:
            self.available_unit_ids[type_].remove(id_)
            self.available_unit_num[type_] -= 1

    def pop_unit(self, type_):
        id_ = self.pt_ids[type_].pop()
        if id_ in self.available_unit_ids[type_]:
            self.available_unit_ids[type_].remove(id_)
            self.available_unit_num[type_] -= 1
        return id_

    # 巡逻打击，轰炸机，歼击机自动打击周围攻击范围内的敌方单位
    def make_patrol_attack(self, attack_list, grand_target_types=None, air_target_types=None):
        if grand_target_types is None:
            grand_target_types = self.GRAND_ATTACK_TYPES  # 对地打击优先级列表
        if air_target_types is None:
            # 对空打击优先级列表
            air_target_types = self.RED_A2A_ATTACK_TYPES if self.side == 'red' else self.BLUE_A2A_ATTACK_TYPES

        cmds = []
        a2a_enemy_set = set()  # 空中被打击目标集合
        a2g_enemy_set = set()  # 在地被打击目标集合

        a2a_unit_set = set()  # 参加空中打击的我方单位
        a2g_unit_set = set()  # 参加对地打击的我方单位

        rm_list = []

        # 歼击机发起攻击任务
        for id_ in self.pt_ids[UnitType.A2A]:
            unit = self.unit_ob.id_map[self.side][id_]
            for type_ in air_target_types:
                for en_id in self.unit_ob.ids[self.en_side][type_]:
                    en_unit = self.unit_ob.id_map[self.en_side][en_id]
                    dist = unit.compute_2d_distance_unit(en_unit)
                    if dist < 120000:
                        a2a_enemy_set.add(en_id)
                        a2a_unit_set.add(id_)

        a2a_enemy_list = list(a2a_enemy_set)
        a2a_unit_list = list(a2a_unit_set)

        for id_ in a2a_unit_list:
            np.random.shuffle(a2a_enemy_list)
            for en_id in a2a_enemy_list:
                # success = attack_list.add_attack(id_, en_id)  # 如果是重复打击则不予添加，不过对于歼击机是否这样还待定
                # if success:
                cmds.append(EnvCmd.make_airattack(id_, en_id, 1))  # 不管是否重复，直接打击
            if id_ in self.available_unit_ids[UnitType.A2A]:
                rm_list.append(id_)
                self.available_unit_num[UnitType.A2A] -= 1
        for id_ in rm_list:
            self.available_unit_ids[UnitType.A2A].remove(id_)

        # 轰炸机发起攻击任务
        rm_list.clear()
        for id_ in self.pt_ids[UnitType.A2G]:
            unit = self.unit_ob.id_map[self.side][id_]
            for type_ in grand_target_types:
                for en_id in self.unit_ob.ids[self.en_side][type_]:
                    en_unit = self.unit_ob.id_map[self.en_side][en_id]
                    dist = unit.compute_2d_distance_unit(en_unit)
                    if dist < 120000:  # 修改了
                        a2g_enemy_set.add(en_id)
                        a2g_unit_set.add(id_)

        a2g_enemy_list = list(a2g_enemy_set)
        a2g_unit_list = list(a2g_unit_set)
        for id_ in a2g_unit_list:
            np.random.shuffle(a2g_enemy_list)
            for en_id in a2g_enemy_list:
                unit = self.unit_ob.id_map[self.side][id_]
                en_unit = self.unit_ob.id_map[self.en_side][en_id]
                success = attack_list.add_attack(id_, en_id)
                if success:  # 如果是重复打击则不予添加
                    cmds.append(EnvCmd.make_targethunt(id_, en_id, unit.compute_2d_attack_angle_unit(en_unit), 90))
            if id_ in self.available_unit_ids[UnitType.A2G]:
                rm_list.append(id_)
                self.available_unit_num[UnitType.A2G] -= 1
        for id_ in rm_list:
            self.available_unit_ids[UnitType.A2G].remove(id_)
        return cmds

    # 选定的类型全部停止作战移动到目标点
    def make_formation_move(self, x, y, z, attack_list, types=None):
        if types is None:
            types = [UnitType.AWACS, UnitType.DISTURB, UnitType.A2G, UnitType.A2A]
        cmds = []
        for type_, id_list in self.pt_ids.items():
            if type_ not in types:
                continue
            for id_ in id_list:
                attack_list.remove_unit(id_)
                cmds.append(EnvCmd.make_areapatrol(id_, x, y, z, *AREA_PATROL_PARAMS))
        for type_ in types:
            self.available_unit_ids[type_] = deepcopy(self.pt_ids[type_])
        for type_, ids in self.available_unit_ids.items():
            self.available_unit_num[type_] = len(ids)
        return cmds

    # 返回是所选单位单位是否都不在作战状态
    def is_units_available(self, types=None):
        if types is None:
            types = [UnitType.A2A, UnitType.A2G]
        total_battle_unit_num = 0
        available_battle_unit_num = 0
        for type_ in types:
            total_battle_unit_num += len(self.pt_ids[type_])
            available_battle_unit_num += self.available_unit_num[type_]
        return total_battle_unit_num == available_battle_unit_num

    def print_formation(self):
        print(self.name)
        print(self.pt_ids)

    # 计算重心坐标, 如果flag = 1,则只计算不在作战任务的单位的中心坐标
    def compute_2d_center_point(self, types=None, mode=0):

        id_map = self.unit_ob.id_map[self.side]
        x_avl, x_count, y_avl, y_count = 0, 0, 0, 0

        if types is None:
            types = [UnitType.AWACS, UnitType.DISTURB, UnitType.A2G, UnitType.A2A]
        for type_, id_list in self.pt_ids.items():
            if type_ not in types:
                continue
            for id_ in id_list:
                if mode == 1 and id_ not in self.available_unit_ids[type_]:
                    continue
                unit = id_map[id_]
                x_avl += (unit.x - x_avl) / (x_count + 1)
                y_avl += (unit.y - y_avl) / (y_count + 1)
        return x_avl, y_avl

    # 返回编队中选定的种类的我方单位是否达到既定点,z轴不需要
    def has_all_arrived(self, x, y, z=0, types=None):
        if types is None:
            types = [UnitType.AWACS, UnitType.DISTURB, UnitType.A2G, UnitType.A2A]
        center_x, center_y = self.compute_2d_center_point(types)
        # print("has_all_arrived : center point", center_x, center_y)
        # print("has_all_arrived : aid point", x, y)
        # print('has_all_arrived : team_name', self.name, 'arr_dis', compute_2d_distance_point(center_x, center_y, x, y))
        return compute_2d_distance_point(center_x, center_y, x, y) < 30000

    # 编队重命名
    def rename(self, name, formation_map):
        formation_map[name] = self
        del formation_map[self.name]
        self.name = name

    # 判断指定类型的单位的数目和是否为0
    def is_empty(self, types=None):
        if types is None:
            types = [UnitType.AWACS, UnitType.DISTURB, UnitType.A2G, UnitType.A2A]
        for type_, id_list in self.pt_ids.items():
            if type_ not in types:
                continue
            if len(id_list) is not 0:
                return False
        return True

    # 返回指定类型的所有id
    def ids(self, types=None):
        ids = []
        if types is None:
            types = [UnitType.AWACS, UnitType.DISTURB, UnitType.A2G, UnitType.A2A]
        for type_, id_list in self.pt_ids.items():
            if type_ not in types:
                continue
            ids.extend(id_list)
        return ids

    # 向self.reside_point前进遇到敌人就打击，没有打击任务就继续前进，
    # 只能适用于只含有单个攻击种类单位的编队,只操控攻击单位，其他单位行动在函数外控制
    def make_line_hunt(self, type_, attack_list, types=None):
        cmds = []
        cmds_ = []
        print("make_line_hunt : f_name", self.name)
        print("make_line_hunt : reside_point", self.reside_point)

        if types is None:
            if type_ == UnitType.A2A:
                types = self.RED_A2A_ATTACK_TYPES if self.side == 'red' else self.BLUE_A2A_ATTACK_TYPES
            if type_ == UnitType.A2G:
                types = self.GRAND_ATTACK_TYPES
            else:
                return cmds

        if type_ == UnitType.A2G:
            cmds_ = self.make_patrol_attack(attack_list, types, [])
        elif type_ == UnitType.A2A:
            cmds_ = self.make_patrol_attack(attack_list, [], types)
        else:
            return cmds
        print('make_line_hunt : beat_cmds', cmds_)
        cmds.extend(cmds_)
        print('make_line_hunt : for_st', self.st)
        if len(cmds_) > 0:  # 执行打击命令则停止前进
            self.st = FormationStatus.TARGET_HUNT
            print("make_line_hunt : st", self.st)
            return cmds
        print("make_line_hunt : avai", self.is_units_available([type_]))
        if self.st in [FormationStatus.TARGET_HUNT, FormationStatus.READY] and self.is_units_available([type_]):
            self.st = FormationStatus.MOVING_2_POINT
            print("make_line_hunt : st", self.st)
            cmds.extend(self.make_formation_move(*self.reside_point, attack_list))
            return cmds
        return cmds

    # 返回在开往驻扎点的路途中会遇到的敌方舰船以及我方现在为止距对方的距离
    def get_encounter_ship_unit_and_gap_dis(self):
        en_id_map = self.unit_ob.id_map[self.en_side]
        en_ship_ids = []
        en_ship_ids.extend(self.unit_ob.ids[self.en_side][UnitType.SHIP])
        en_ship_ids.extend(self.unit_ob.ids[self.en_side][UnitType.UNSGTAR])  # 情报中未识别单位是敌船

        en_ship1_unit = None if len(en_ship_ids) < 1 else en_id_map[en_ship_ids[0]]
        en_ship2_unit = None if len(en_ship_ids) < 2 else en_id_map[en_ship_ids[1]]
        present_point = self.compute_2d_center_point()
        # print("step:en_ids\n", self.unit_ob.ids[self.en_side])
        # print("-------")
        # print("step: en_ship1_unit is None", en_ship1_unit is None)
        # print("step: en_ship2_unit is None", en_ship2_unit is None)

        x1, y1 = present_point[0], present_point[1]
        x2, y2 = self.reside_point[0], self.reside_point[1]

        flag1 = will_attack_by_ship(x1, y1, x2, y2, en_ship1_unit.x,
                                    en_ship1_unit.y) if en_ship1_unit is not None else False
        flag2 = will_attack_by_ship(x1, y1, x2, y2, en_ship2_unit.x,
                                    en_ship2_unit.y) if en_ship2_unit is not None else False

        attack_ship_unit = None
        if flag1 and flag2:
            dis1 = en_ship1_unit.compute_2d_distance(x1, y1)
            dis2 = en_ship2_unit.compute_2d_distance(x1, y1)
            attack_ship_unit = en_ship1_unit if dis1 < dis2 else en_ship2_unit
        elif flag1 and not flag2:
            attack_ship_unit = en_ship1_unit
        elif not flag1 and flag2:
            attack_ship_unit = en_ship2_unit
        gap_dis = 0
        if attack_ship_unit is not None:
            gap_dis = attack_ship_unit.compute_2d_distance(x1, y1)
        return attack_ship_unit, gap_dis

    def step(self, attack_list=None, types=None, mode=0):
        cmds = []
        return cmds
