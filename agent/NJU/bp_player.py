from .agent_connector import AgentConnector
from .config.config import config as bp_config


class AgentPlayer:
    def __init__(self, name, config):
        """
        最佳实践对接对战框架接口
        本接口作为网络推演与对战框架的适配器，故单个实例只支持单个player。若要使用多个player，请在对战框架中使用多次
        需要注意的是，这里的 name == 战队名称（对战框架中战队名称） == config设置中，player的名称
        """
        self.name, self.side = name, config['side']

        model_path = r'agent\agent_8v8_v2\model\red_player-checkpoint-470'   # 载入模型的地址，根据需要根据各自运行情况做修改
        # model_path = None   # 载入模型的地址，若不载入，则设为 None
        self.agent_connector = AgentConnector(
            name,
            config,
            bp_config['gear_config'],
            bp_config['training_config'],
            model_path
        )

    def reset(self):
        self.agent_connector.reset()

    def step(self, sim_time, obs, **kwargs):
        return self.agent_connector.step(sim_time, obs)
